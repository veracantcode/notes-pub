

## Backlinks

> - [](corrective napping.md)
>   - I suffer from [[insomnia]] and I've been trying to come up with solution to help with this. One method has been using [[psilocybin mushrooms]] another tactic has been trying to math out how many hours I should sleep in a day and then using that to create an algorithm to determine a corrective sleep schedule. So far, I've yet to determine such algorithm, but I am hopeful such a thing is knowable.
>    
> - [2022-01-22](2022-01-22.md)
>   - I missed [[flancia meet]] this morning. I was too tired and was sleeping lol. At least I had some good sleep. I suffer from pretty bad [[insomnia]] and I'll go days without sleeping. It looks like [[esperanto]] came up which is an interesting throwback. I used to study esperanto back in the day with friends and I'm somewhat fluent.
>    
> - [2022-01-18](2022-01-18.md)
>   - [[insomnia]]

_Backlinks last generated 2022-01-28 07:55:20_
