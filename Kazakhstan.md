Kazakhstan experiencing civil unrest. On 25 October 1990, Kazakhstan declared its sovereignty on its territory as a republic within the [[Soviet Union]]. In January 2022, the country plunged into unrest following a spike in fuel prices. https://en.wikipedia.org/wiki/2022_Kazakh_protests 

## Backlinks

> - [](2022-01-09.md)
>   - [[Kazakhstan]] is experiencing civil unrest which is affecting the [[crypto market]]

_Backlinks last generated 2022-01-28 07:55:20_
