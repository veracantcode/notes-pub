Created this node because why not.

## Backlinks

> - [](2021-01-18.md)
>   - -	In my opinion, the reason we can’t challenge the ruling powers directly is that the people most affected by [[capitalism]] don’t have solidarity with each other. Therefore, the one indispensable precondition for long-term victory is to establish solidarity. Solidarity in the real and practical struggles regular people actually face, building power from below until no power from above can stop it.
>    
> - [](2021-05-04.md)
>   - nfts offer a new model that don't depend on [[capitalism]] and surveilence
>    
> - [](2021-01-16.md)
>   - There is a ton of [[gatekeeping]] for research papers. I blame [[capitalism]]
>    
> - [](pay to win.md)
>   - One of my favorite nicknames for [[capitalism]]

_Backlinks last generated 2022-01-28 07:55:20_
