1.  [[CTZN]] (https://ctznry.com/): The absolute easiest and safest Facebook alternative to join. Joining the main community is little different than joining Facebook and it guides you through making your own.
2.  [[Mastodon]] (https://joinmastodon.org/): Twitter alternative whose [communities](https://joinmastodon.org/communities/general) have been rapidly growing in popularity.
3.  [[Friendica]] (https://friendi.ca/): the most user-friendly Facebook alternative with lots of colorful [servers](https://dir.friendica.social/servers).
4.  [[Pleroma]] (https://pleroma.social/): a network of Facebook-like [instances](https://pleroma.social/#featured-instances). Make it even more user-friendly by adding [Soapbox](https://soapbox.pub/).
5.  [[Diaspora]] (https://diasporafoundation.org/): a Facebook-like system that has been around a long time and has many active [pods](https://diaspora.podupti.me/).
6.  [[Funkwhale]] (https://funkwhale.audio/en_US/): a SoundCloud-like social network for sharing and enjoying music that also uses [pods](https://funkwhale.audio/en_US/#get-started).
7.  [[Plume]] (https://joinplu.me/): a social network for blog posts. ([See the public instance](https://fediverse.blog/))
8.  [[PeerTube]] (https://joinpeertube.org/): a network of YouTube-like [instances](https://joinpeertube.org/instances#instances-list). (Personal favorite is the educational [TILvids](https://tilvids.com/) instance)
9.  [[Pixelfed]] (https://pixelfed.org/): a network of Instagram-like [instances](https://beta.joinpixelfed.org/).
10.  [[Hubzilla]] (https://hubzilla.org//page/hubzilla/hubzilla-project): arguably the most feature-rich Facebook alternative with lots of different [hubs](https://the-federation.info/hubzilla).

## Backlinks

> - [](immer protocol.md)
>   - >This is the keystone of the [[metaverse]]. It connects Immersive Web experiences together by giving them a common language so that users (immersers) can bring their identities and avatars with them wherever they travel, and they can pick up friends and trophies along the way, but without dictating anything about how each world works or what technology it uses. Immersers will be able to broadcast their location to friends so that, with the simple click of a link, they can instantly join anywhere in [[immers space]]. We’re building it open-source, making it easy to plug in to all kinds of experiences, and using the ActivityPub open Web standard which means it will also [link with popular [[federated social media networks]] like [[mastodon]]](https://blog.joinmastodon.org/2018/06/why-activitypub-is-the-future/).

_Backlinks last generated 2022-01-28 07:55:20_
