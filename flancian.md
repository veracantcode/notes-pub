- [flancian@twitter](https://twitter.com/flancian)
- [In Flancia we will meet (@flancian@social.coop) - social.coop](https://social.coop/@flancian)

- #protopian 
- #anarchist

[[2021-06-23]]

## Backlinks

> - [](distributed press 2021-05-19.md)
>   - [[flancian]]
>   - [[flancian]]
>   - [[flancian]]
>    
> - [](2021-01-22.md)
>   - Talked to [[flancian]] about [[note blocks]] and semantic structuring
>    
> - [](2020-12-30.md)
>   - met [[flancian]] who works on above project, seems pretty cool
>    
> - [](2021-02-08.md)
>   - [[flancian]] posted something on [[twitter]] today that really resonated with me
>    
> - [List of anarchists](anarchists.md)
>   - ![[flancian]]
>    
> - [](2021-05-23.md)
>   - talked with [[flancian]]
>    
> - [Ocell](ocell.md)
>   - [[flancian]] https://twitter.com/flancian/status/1325024046346539008
>    
> - [](2021-06-22.md)
>   - [[flancian]] asked if I was noting today. I guess this answer's that question lol
>   - [[flancian]] is giving a talk at [[i annotate]] conference today
>    
> - [](agorans.md)
>   - -	[[flancian]] CET
>    
> - [](2021-02-02.md)
>   - I'm journaling thanks to [[flancian]] encouraging me.
>    
> - [](2021-01-10.md)
>   - found [[spacehey]] from [[flancian]]. Looks cool. My gay ass already found some cute girls on there :halo:
>    
> - [](2021-01-11.md)
>   - I really like the formatting changes [[flancian]] has done
>    
> - [](2021-01-19.md)
>   - TIL [[flancian]] is in [[CET]] which is the same timezone as Paris and Rome
>    
> - [](2021-04-08.md)
>   - Woke up early today to listen to [[flancian]] on [[pkmchat]]
>    
> - [](2021-01-15.md)
>   - Just realized [[flancian]] auto embeds tweets into [[agora]] via [[twitter]] urls
>    
> - [](2021-04-19.md)
>   - I talked to [[flancian]] a lot about possibly using [[ctzn]] as a [[data store]] and identity layer. I need to research the [[whitepaper]] some more to learn the nuances
>    
> - [](anti-dictionary.md)
>   - idea I've been mulling over lately. Take for example the concept of a dictionary: each entry has a canonical defintion of what people think that word means. wikipedia works the same way. but an anti-dictionary is a tool that allows the inverse of this, you select an identifier and then it gives you a list of what different people think this means. I consider [[agora]] to be an anti-dictionary. I wonder what [[flancian]] thinks of this.
>    
> - [](foam.md)
>   - [[flancian]] used to use this, now uses [[obsidian]]
>    
> - [](bookmarks.md)
>   - -	[[todo]] ([[flancian]] uses [[do]] I believe)
>    
> - [](tags.md)
>   - e.g. my #friend is [[flancian]]
>    
> - [](found in the agora.md)
>   - concept that [[flancian]] and I came up with where we list interesting things we find in the [[agora]]
>    
> - [](logseq/journals/2021_07_20.md)
>   - talked with [[flancian]] and [[benhylau]] about clojure
>    
> - [](logseq/journals/2021_08_18.md)
>   - worked on [[agora bridge]] with [[flancian]] today
>    
> - [](logseq/pages/flancia social impact.md)
>   - We might want to add something about "thriving" in there h/t [[flancian]]

_Backlinks last generated 2022-01-28 07:55:20_
