- These are instructions on how to add your [[digital-garden]] to the highly experimental interlay version of the [[agora]]
	- If you have any questions feel free to contact me
		- #email veronika.m.winters@gmail.com
		- #twitter @flanciavera
		- #matrix vera@fairydust.space
	- I'll be happy to add you manually on request

- Fork our repo on github https://github.com/flancia-coop/agora
- clone your repo
	- `git clone <url to your fork>`
- cd into repo
	- `cd agora`
- import your own repo as a submodule
	- `git submodule add <url to your repo> <your desired name>`
- add files to index
	- `git add .`
- create commit
	- `git commit -m "submodule"`
- push changes back to your fork
	- `git push origin master`
- create pull request 

[//begin]: # "Autogenerated link references for markdown compatibility"
[digital-garden]: digital-garden.md "digital-garden"
[agora]: agora.md "agora"
[//end]: # "Autogenerated link references"

## Backlinks

> - [](agora interlay graph.md)
>   - [[agora interlay howto]]

_Backlinks last generated 2022-01-28 07:55:20_
