- ORID Is a specific facilitation framework that enables a focused conversation with a group of people in order to reach some point of agreement or clarify differences.
- #go https://www.betterevaluation.org/en/evaluation-options/orid
- #open-goverance

## Backlinks

> - [](patcon 2021-05-21.md)
>   - [[orid]]

_Backlinks last generated 2022-01-28 07:55:20_
