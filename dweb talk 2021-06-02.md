- nathan
	- ESOP
	- exit to comnmunity
	- zebras unite
- guo
	- matters.news
	- mostly chinese speaking community
	- ipfs
	- likecoin
	- content moderation
		- jury system

- social roots
	- coordination at scale
	- social protocols
		- conventions for social behavior
		- permissions
		- visible org structure helps coordination

- hyper hyper space
	- p2p in the browser

- mix
	- ancestry.com meets ssb
	- identity and lineage


- mai
	- hyperlocal tech
	- don't need to take over the world
	- centralized coordination
		- decentralized power

## Backlinks

> - [](distributed interest orgs.md)
>   - [[dweb]]
>    
> - [what do we want from the [[agora]] interlay?](2021-W23.md)
>   - [[dweb talk 2021-06-02]]
>    
> - [](2021-05-06.md)
>   - checking out [[vroid]] it's a [[vr]] studio for editing [[vrm]] avatars. [[jin]] had one during the [[dweb]] conference call

_Backlinks last generated 2022-01-28 07:55:20_
