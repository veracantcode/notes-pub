- A platform for deciding the outcome of proposals
- similar to [[loom.io]]
- have an [[agora action]] to vote on a proposal
	- maybe like #vote-yes or something similar


- Looking for software tools in [[open source democracy]]

[//begin]: # "Autogenerated link references for markdown compatibility"
[open source democracy]: open source democracy.md "open source democracy"
[//end]: # "Autogenerated link references"

## Backlinks

> - [](2021-05-19.md)
>   - I want to work on [[agora proposal platform]]
>    
> - [2022-01-27](todo.md)
>   - build [[agora proposal platform]]
>   - build [[agora proposal platform]]
>    
> - [](liquid democracy.md)
>   - [[agora proposal platform]]

_Backlinks last generated 2022-01-28 07:55:20_
