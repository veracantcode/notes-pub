[[blockchain technology]] looks like a competitor to [[cosmos network]]

Polkadot is a network protocol that allows arbitrary data—not just tokens—to be transferred across blockchains.


[//begin]: # "Autogenerated link references for markdown compatibility"
[cosmos network]: cosmos network.md "cosmos network"
[//end]: # "Autogenerated link references"

