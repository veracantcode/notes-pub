Lots of features. It lets you create a user wiki

#go https://www.notion.so/

## Backlinks

> - [](2021-04-14.md)
>   - -	[[notion]]
>    
> - [2022-01-27](todo.md)
>   - build [[notion]] integration
>   - build [[notion]] integration
>    
> - [](2021-04-24.md)
>   - I'm downloading a bunch of note apps to see what works on mobile. I signed up for [[notion]] [[api]] so we'll see what happens with that
>    
> - [](preliterate society.md)
>   - Doubtless many "truths" that preliterate peoples held were patently false, a statement that is easily made nowadays. But I will make a case for the [[notion]] that their outlook, particularly as applied to their communities' relationship with the natural world, had a basic soundness— one that is particularly relevant for our times.
>    
> - [](mobile agora client.md)
>   - [[notion]] (mobile app is super slow)
>    
> - [](2021-05-05.md)
>   - Started using [[notion]] for private notes

_Backlinks last generated 2022-01-28 07:55:20_
