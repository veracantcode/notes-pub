- [[jargon]] in [[software development]] that means to make a copy of someone's code to make changes
-  [[github]]

## Backlinks

> - [](2021-05-12.md)
>   - I'm working on a [[fork]] that adds [[ctzn]] login functionality to the [[agora]]. My hope is that we can leverage that tech to make a mobile-friendly read/write interface to the [[agora]]

_Backlinks last generated 2022-01-28 07:55:20_
