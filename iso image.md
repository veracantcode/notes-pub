A [[disk image]] for burning to media

## Backlinks

> - [](2021-05-07.md)
>   - Found [[ventoy]] that let's you put multiple [[iso image]]s on a boot medium like USB or DVD. Really handy to have a diagnostic iso like [[gparted]] in case something happens.
>    
> - [](ventoy.md)
>   - tool to load multiple file system [[iso image]]s

_Backlinks last generated 2022-01-28 07:55:20_
