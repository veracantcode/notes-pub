A way of doing something that may be popular but is wrong based on collective research

Tags are example of this
-	All items with a given tag are presented as being related… but it’s hard to see how. They’re just a jumbled, unordered list.

resetting form field information after an invalid form submission

## Backlinks

> - [](2021-01-11.md)
>   - I keep accidentally closing a tab instead of clicking on it. Seems like a design [[anti-pattern]]

_Backlinks last generated 2022-01-28 07:55:20_
