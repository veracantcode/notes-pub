the DNS search domain is what the DNS service will use to resolve hostnames that are not fully qualified.

## Backlinks

> - [what do we want from the [[agora]] interlay?](2021-W23.md)
>   - [[dns search domain]]

_Backlinks last generated 2022-01-28 07:55:20_
