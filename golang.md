Programming language from google that I use extensively

## Backlinks

> - [](vlang.md)
>   - similar to [[golang]]
>    
> - [](go.md)
>   - the [[golang]] programming language
>    
> - [](gomuks.md)
>   - [[golang]]
>    
> - [](cosmos network.md)
>   - uses [[golang]]

_Backlinks last generated 2022-01-28 07:55:20_
