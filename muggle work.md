 [[muggle job]]

A job that is a regular 9-5, especially if one has a more exotic night job, e.g. stripper

## Backlinks

> - [](2021-02-02.md)
>   - I've been trying to find [[muggle work]] but it's been a chore. I've been looking at [[light industrial work]] since that is usually pretty mindless. I live mindless labor. I feel like I can just dissociate and let the day go by lol

_Backlinks last generated 2022-01-28 07:55:20_
