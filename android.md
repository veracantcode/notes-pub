- [[mobile phone]] platform from [[google]]

## Backlinks

> - [](2021-04-14.md)
>   - playing with new software for [[android]]
>   - I'm looking for a good [[agora]] app for [[android]]
>    
> - [](2020-12-30.md)
>   - Need to find a way to get daily journal/git integration on [[android]] device
>   - Ooooh derp I was turning ON notifications for [[android]].
>    
> - [](2020-12-31.md)
>   - started using [[xmpp]] again with [[conversations]] on [[android]] and [[dino]] on desktop
>    
> - [](conversations.md)
>   - [[android]] [[xmpp]] client
>    
> - [](ebook resources.md)
>   - I use [[moon reader]] on [[android]] and I'm currently trying out [[calibre]] on linux
>    
> - [](launchy.md)
>   - [[windows 10]] and [[android]]
>    
> - [](ATAK.md)
>   - [[android]] Team Awareness Kit
>   - [[android]] app to map out terrain and mark resources like medics and vehicles
>    
> - [](2021-03-25.md)
>   - I'm sad that [[obsidian]] mobile has decided to go for a for-pay model for their beta [[android]] client. Oh well. I might end up buying a subscription, we'll see

_Backlinks last generated 2022-01-28 07:55:20_
