- A strongly defined, highly compatible specification of Markdown
	- an unambiguous definition, the original markdown contained multiple ambiguities
- #go https://commonmark.org/
- parser included in [[atjson]]
- 

## Backlinks

> - [](2021-07-11.md)
>   - [[commonmark]]

_Backlinks last generated 2022-01-28 07:55:20_
