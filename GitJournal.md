I like that gitjournal creates "layers" so I can just hit x to go back

## Backlinks

> - [](2021-01-04.md)
>   - I wish there was a better [[roam-like]] for mobile. I'm using [[GitJournal]] and it's a little too limited.
>    
> - [](2021-01-11.md)
>   - Journaling in [[GitJournal]] kinda sucks. I need to figure out a better flow so I actually take notes. Maybe I just have a rough version at first and then tend with [[obsidian]]?
>    
> - [](mobile agora client.md)
>   - [[GitJournal]] (not my favorite),

_Backlinks last generated 2022-01-28 07:55:20_
