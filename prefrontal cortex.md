

## Backlinks

> - [](depression.md)
>   - It is widely accepted that depression is caused by neuronal atrophy in [[prefrontal cortex]] (PFC) as a result of stress [https://www.nature.com/articles/s41398-021-01312-y]
>    
> - [](microdosing.md)
>   - [[psilocybin]] has been shown to give [[neuroplasticity]] to neurons in the [[prefrontal cortex]]

_Backlinks last generated 2022-01-28 07:55:20_
