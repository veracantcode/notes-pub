[![montage of a bunch of cute dog pictures in one image](https://i.kym-cdn.com/photos/images/newsfeed/000/317/538/4da.png "4da.png")](https://knowyourmeme.com/photos/317538-dogs)

## About

The **dog** is a furry, omnivorous mammal widely regarded as the first [animal](https://knowyourmeme.com/memes/subcultures/animals) to have been domesticated by humans to assist in field labor, such as herding livestock and hunting, as well as to serve as a companion. Along with the [cat](https://knowyourmeme.com/memes/subcultures/cats), the [dog](https://knowyourmeme.com/memes/cultures/dogs) has long been one of the most searched pet animals in the world and a popular subgenre of animal-themed [viral videos](https://knowyourmeme.com/memes/viral-videos), mainly due to their de facto status as the two most popular choices of mammals for domesticated pets.

## Online History

**\[researching; [request editorship](https://knowyourmeme.com/memes/dogs/editorships/ask) to join the research efforts!\]**

#### "Nobody Knows You're a Dog"

On July 5th, 1993, The New Yorker published a cartoon by artist Peter Steiner, which featured an illustration of a dog seated at a computer telling his canine companion that “on [the Internet](https://knowyourmeme.com/memes/cultures/the-internet), nobody knows you’re a dog” (shown below). Years later on December 14th, 2000, The New York Times published an interview with Steiner in an article titled “Cartoon Captures Spirit of the Internet,” noting that the cartoon did not receive much attention initially, but steadily grew in popularity over many years.

## Online Presence

**\[researching; [request editorship](https://knowyourmeme.com/memes/dogs/editorships/ask) to join the research efforts!\]**

## Fandom

#### The World's Ugliest Dog Contest

[The World's Ugliest Dog Contest](https://knowyourmeme.com/memes/events/worlds-ugliest-dog-contest) is an annual contest that takes place at the Sonoma-Marin County Fair where dogs compete for the titular crown. The contest, which has been running since the 1970s, has caused several unconventional-looking dogs to become famous. Each winner receives a $1000 prize and a trophy.[\[2\]](https://knowyourmeme.com/memes/subcultures/dogs#fn2)

## Notable Dogs

#### Birthday Dog

[![Birthday dog in front of his cake, ready to enjoy it](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Birthday doggo")](https://knowyourmeme.com/photos/308466-dogs)

**[Birthday Dog](https://knowyourmeme.com/memes/birthday-dog)** is the nickname given to Riley, a bichon frise/poodle mix dog, after a photo of him sitting in a human-like pose at a kitchen table on his birthday was shared on [Reddit](https://knowyourmeme.com/memes/sites/reddit) in February 2010. The dog is known for his unique facial expression that makes him appear to be under the influence.

#### Sam the World's Ugliest Dog

**[Sam](https://knowyourmeme.com/memes/sam-the-world-s-ugliest-dog)** was a blind purebred hairless [Chinese](https://knowyourmeme.com/memes/cultures/china) Crested owned by Susie Lockheed of Santa Barbara, CA. won the title of World’s Ugliest Dog from 2003-2005. Due to heart complications, Sam was euthanized on November 18, 2005, just shy of his 15th birthday. Shortly after, images of the dog went viral, popping up on [Myspaces](https://knowyourmeme.com/memes/sites/myspace) and forums worldwide.

[![Demotivational poster style meme of Sam, the worlds ugliest dog](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "1b0.jpg")](https://knowyourmeme.com/photos/308467-dogs)

#### Broccoli Dog

**[Broccoli Dog](https://knowyourmeme.com/memes/broccoli-dog)** is the nickname given to an image of a dog sitting at a table with a plateful of broccoli, in which he takes an understandable disinterest.

[![Dog refusing to look like he would enjoy that broccoli](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "9d0.jpg")](https://knowyourmeme.com/photos/308474-dogs)

#### Riot Dog

**[Riot Dog](https://knowyourmeme.com/memes/riot-dog)** is a nickname given to both Kanellos (“Cinnamon” in Greek) and Loukaniko (“Sausage” in Greek), two stray dogs from Athens that became famous for their nearly ubiquitous presence during every major protest in Greece since 2008.

[![Riot dogs from the Athens riots Kanellos and Loukaniko](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "6e0.jpg")](https://knowyourmeme.com/photos/308476-dogs)

#### Cool Dog

**[Cool Dog](https://knowyourmeme.com/memes/cool-dog)** is a nickname given to a leashed Shiba Inu dog with an elbow bent over what looks like a doghouse roof. Because of its awesomely cool-headed posture, the image gained traction on [imageboard](https://knowyourmeme.com/memes/subcultures/imageboard) sites like [4chan](https://knowyourmeme.com/memes/sites/4chan) and later evolved into an Advice Dog spinoff which is currently ranked #26 on [MemeGenerator’s](https://knowyourmeme.com/memes/sites/meme-generator) Top Tier. He has also been seen [photoshopped](https://knowyourmeme.com/memes/sites/photoshop) into various “cool” places.

[![Shiba Inu Catahoula Cur American Eskimo Dog Puppy dog dog like mammal dog breed dog breed group sakhalin husky](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "d6e.jpg")](https://knowyourmeme.com/photos/308455-dogs)

#### Cupcake Dog

**[Cupcake Dog](https://knowyourmeme.com/memes/cupcake-dog)** is the nickname given to “Staines”, an [Australian](https://knowyourmeme.com/memes/cultures/australia) Shepard dog who made his TV debut on Animal Planet’s dog training show “It’s Me or the Dog.” In early 2009, the dog gained internet fame for its hypnotic staring at a plate full of cupcakes after the footage was uploaded onto [YouTube](https://knowyourmeme.com/memes/sites/youtube).

[![Cool dog just chilling](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "a51.jpg")](https://knowyourmeme.com/photos/308485-dogs)

#### The Call of Duty Dog

**The _Call of Duty_ Dog** is the nickname given to Riley, a German Shepherd character featured in the official trailer for [_Call of Duty: Ghosts_](https://knowyourmeme.com/memes/subcultures/call-of-duty) during [Microsoft](https://knowyourmeme.com/memes/subcultures/microsoft)'s [Xbox One](https://knowyourmeme.com/memes/subcultures/xbox) conference in May 2013.

[![Call of Duty: Ghosts Call of Duty: WWII German Shepherd Call of Duty: Modern Warfare 3 Call of Duty: Infinite Warfare Destiny dog dog breed dog like mammal snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Call of Duty Dog")](https://knowyourmeme.com/photos/1010103)

#### Menswear Dog

[Menswear Dog](https://knowyourmeme.com/memes/menswear-dog) is the nickname of a Shibu Inu dog owned by New York City residents David Fung and Yena Kim, that is known for being photographed while wearing men’s apparel.

[![Shiba Inu dog like mammal dog dog breed mammal dog breed group snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "b5d.jpg")](https://knowyourmeme.com/photos/685182-menswear-dog) [![Shiba Inu dog like mammal dog dog breed mammal dog breed group shiba inu shikoku dingo snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "4f5.jpg")](https://knowyourmeme.com/photos/685180-menswear-dog)

  

#### Earl the Grumpy Dog

[**Grumpy Puppy**](https://knowyourmeme.com/memes/earl-the-grumpy-puppy) is a nickname given to Earl, a puggle (pug-and-beagle hybrid) from Davenport, Iowa who became the subject of online fame for its adorably irritated-looking facial expressions, in a similar vein to those worn by none other than [Grumpy Cat](https://knowyourmeme.com/memes/grumpy-cat).

[![Puggle Pug Beagle dog dog like mammal dog breed mammal grass](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Meet Earl")](https://knowyourmeme.com/photos/998749-earl-the-grumpy-puppy)

  

#### Tonkey Bear

[**Tonkey**](https://knowyourmeme.com/memes/tonkey-bear), also nicknamed "Tonkey Bear" and "Bear Coat Tonkey," is a bear coat Shar Pei puppy from Edmonton, [Canada](https://knowyourmeme.com/memes/cultures/canada) who rose to viral fame online after several photographs of the fluffy, bear-like puppy went viral on [Instagram](https://knowyourmeme.com/memes/sites/instagram) and [Facebook](https://knowyourmeme.com/memes/sites/facebook) in July 2015. The dog was born in March 2015 in Edmonton, Canada, where she lives with her caretakers Christine Park and her boyfriend Dave Ngu. On May 31st, Park created an Instagram account for Tonkey with a portrait photograph of her, garnering more than 183,000 followers over the course of the next two months.

[![Shar Pei Bear Puppy dog dog like mammal dog breed dog breed group snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Tonkey Pose")](https://knowyourmeme.com/photos/999067)

  

#### Advice Dog

**[Advice Dog](https://knowyourmeme.com/memes/advice-dog)** is a popular [image macro](https://knowyourmeme.com/memes/image-macros) series that that features a picture of the head of a smiling puppy on a multicolored color wheel background split into 6 colors. Usual derivatives are often accompanied by two lines of text written in a guidance / advising format with the advice given usually being very poor, unethical or deplorable. Since its debut, the series has spawned dozens of spin-offs referred to as Advice Animals of an animal’s head on a color wheel background or merely just an animal (including humans) with their own archetype personalities using the same two line image macro formula.

[![Dog dog dog like mammal dog breed puppy mammal vertebrate nose labrador retriever retriever snout dog breed group puppy love](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "63f.gif")](https://knowyourmeme.com/photos/308481-dogs)

#### "Yes, This is Dog"

**[Yes, This is Dog](https://knowyourmeme.com/memes/yes-this-is-dog)** is an image macro series featuring a black Labrador [anthropomorphized](https://knowyourmeme.com/memes/cultures/anthropomorphism) with the caption as if it is answering the telephone. The phrase and image have been remixed into a variety of different photographs often including other animals in anthropomorphic situations.

[![HELLO YES, THIS IS DOG Labrador Retriever photo caption dog dog like mammal dog breed group](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "This is Dog")](https://knowyourmeme.com/photos/308470-dogs)

#### I Have No Idea What I'm Doing

**[I Have No Idea What I’m Doing](https://knowyourmeme.com/memes/i-have-no-idea-what-im-doing)** is a [catchphrase](https://knowyourmeme.com/memes/catchphrases) often used in image macro captions featuring photos of animals, typically canines, posed as if they are performing tasks associated with humans.

[![I HAVE NOIDEA WHATIM DOING Golden Retriever Boxer dog dog like mammal dog breed snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "98c.jpg")](https://knowyourmeme.com/photos/308501-dogs)

#### WHARRGARBL / Sprinkler Dog

**[WHARRGARBL](https://knowyourmeme.com/memes/wharrgarbl-sprinkler-dog)** is a caption/image macro that originated in 2008, picturing a dog trying to drink out of a sprinkler.

[![WHARRGARBL Boston Terrier Pet sitting dog breed dog fauna dog like mammal grass photo caption](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "a0c.jpg")](https://knowyourmeme.com/photos/308496-dogs)

#### Insanity Puppy

**[Insanity Puppy](https://knowyourmeme.com/memes/insanity-puppy)** is an advice animal image macro series of a siberian husky puppy with markings reminiscent of a cartoon villain. He is depicted as a younger version of [Insanity Wolf](https://knowyourmeme.com/memes/insanity-wolf), similar to [Baby Courage Wolf](https://knowyourmeme.com/memes/baby-courage-wolf). The text often consists of a over the top or psychotic reaction to something.

[![YOU CALLIT MEME I CALLIT MAIM quickmeme.com Siberian Husky Puppy siberian husky dog dog like mammal sakhalin husky dog breed mammal sled dog vertebrate photo caption](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "57a.jpg")](https://knowyourmeme.com/photos/308503-dogs)

#### Lawyer Dog

**[Lawyer Dog](https://knowyourmeme.com/memes/lawyer-dog)** is an advice animal style image macro series featuring a corgi dressed up in office attire, sitting at a desk with his paw on a book. The captions typically juxtapose legalese with canine-related puns.

[![MAYBE LTS BECAUSE IMA DOG BUTTHIS CASELOOKS PRETTY BLACKAND WHITE TO ME Dog dog dog breed photo caption dog like mammal](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "85a.jpg")](https://knowyourmeme.com/photos/308457-dogs)

#### Depression Dog

**[Depression Dog](https://knowyourmeme.com/memes/depression-dog)** is an advice animal style image macro featuring a cut out photo of a taxidermic dog on color wheel background with shades of gray. The captions typically present bleak situations with a downtrodden outlook, similar to [Forever Alone](https://knowyourmeme.com/memes/forever-alone) image macros.

[![WHEN LIFE GIVES YOU LEMONS REALIZE YOU ARE ALLERGIC TO FRUIT memegonerator.net Dog photo caption dog dog breed snout dog like mammal](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "dc9.jpg")](https://knowyourmeme.com/photos/308465-dogs)

#### Bussiness Dog

**[Bussiness Dog](https://knowyourmeme.com/memes/business-dog)** is an image macro series that that features a picture of a dog with suit. The captions usually includes something an office boss would do or request, but with a canine twist.

[![THIS ISA HUGE OPPORTUNITY FOR THE COMPANY FETCH THE BALLAND RUN WITHIT quickmeme.com Dog dog dog breed dog like mammal photo caption snout retriever](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "7f8.jpg")](https://knowyourmeme.com/photos/308448-dogs)

#### Good Dog Greg

**[Good Dog Greg](https://knowyourmeme.com/memes/good-dog-greg)** is an advice animal image macro series that features a photo of a dog looking up toward the camera accompanied by captions portraying the dog as a selfless benefactor, similar to its precursor [Good Guy Greg](https://knowyourmeme.com/memes/good-guy-greg).

[![NEEDS TO PEE INAITS 4 HOURS TILL YOU GET HOME AND DOES IT OUTISIDE Siberian Husky Labradoodle dog photo caption dog breed dog like mammal snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "1c0.jpg")](https://knowyourmeme.com/photos/308460-dogs)

#### Unimpressed Dog

[**Unimpressed Dog**](https://knowyourmeme.com/memes/unimpressed-dog) is an [advice animal](https://knowyourmeme.com/memes/advice-animals) image macro series featuring a photograph of a dog looking rather aloof set against the backdrop of a beautiful mountain vista atop Mt. Sherman in Colorado. The captions express the animal’s perceived indifference towards things that are deemed impressive, in a similar vein to [Unimpressed Spock](https://knowyourmeme.com/memes/spock-is-not-impressed) and [Unimpressed McKayla](https://knowyourmeme.com/memes/mckayla-is-not-impressed).

[![Alaskan Malamute Puppy sky dog photography](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "906.jpg")](https://knowyourmeme.com/photos/606877)

  

#### Jealous Husky

[**Jealous Husky**](https://knowyourmeme.com/memes/jealous-husky), also known as **Pissed Off Husky**, is an advice animal image macro series featuring a photograph of an annoyed-looking Alaskan Malamute dog seated next to a man and a woman on a snow-covered mountain. The captions typically portray the dog as his owner's overprotective [bro](https://knowyourmeme.com/memes/bro) with guarded feelings towards his girlfriend.

[![WAIT A MINUTE THEY WERENT WRESTLING mernegenerator.net Siberian Husky photo caption dog breed dog breed group](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "606.jpg")](https://knowyourmeme.com/photos/717211-jealous-husky)

  

#### "I Dunno LOL"

[**I dunno LOL**](https://knowyourmeme.com/memes/i-dunno-lol-_o) (emoticon: **¯\\(°\_o)/¯** ) is an expression denoting a sense of confusion, derived from the common English idiom "I don't know" and the acronym [LOL](https://knowyourmeme.com/memes/lol) which stands for "laugh out loud." Sometime in 2007, an image of a smiling dog titled **Happy Dog** was paired with the caption “I Dunno LOL.” The original photograph was first uploaded via Korean imageboard sites as early as in July 2003. The phrase may be more recognizable in its [emoticon](https://knowyourmeme.com/memes/emoticons) form ¯\\(°\_o)/¯, which depicts an [ASCII](https://knowyourmeme.com/memes/ascii-art)\-based character shrugging with arms raised up high.

[![IDUNNO LOL Dog dog dog breed dog breed group dog like mammal canaan dog photo caption](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "i-dunno-lol.jpg")](https://knowyourmeme.com/photos/3309)

  

#### Dog Fort

**[Dog Fort](https://knowyourmeme.com/memes/dog-fort)** is a comics series, that are generally multipanes that include a recon team of dog lobsters, led by team leader Red Lobster, checking in with a communications officer named Dog Fort for further instructions

[![COME IN DOG FORT, OVER... RED LOBSTER ST ANDING BY THIS IS DOG FORT Beagle dog dog breed dog like mammal beagle photo caption](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "e0c.png")](https://knowyourmeme.com/photos/308484-dogs)

#### Techno Dog

**[Techno Dog](https://knowyourmeme.com/memes/techno-dog)** is the name of a video featuring a dog dancing with techno music.

[![TECHNE PEEL THE BERT Cat German Shepherd green yellow mammal fauna small to medium sized cats cat like mammal cat organism](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "f51.png")](https://knowyourmeme.com/photos/308491-dogs)

#### Human Dog

**[Human Dog](https://knowyourmeme.com/memes/human-dog)** is a series of YouTube videos, which show various pet dogs dressed in people’s clothes and doing things like eating a snack or reading the newspaper, with a bit of help from the puppeteer’s hands.

[![Dog dog dog like mammal dog breed](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "510.jpg")](https://knowyourmeme.com/photos/308493-dogs)

#### "My Faggot Dog"

["**My Faggot Dog**"](https://knowyourmeme.com/memes/my-faggot-dog) is the name given to an image and [copypasta](https://knowyourmeme.com/memes/copypasta) of a picture of a dog with a smug expression, accompanied by a pleading call for help. The dog picture has been used as [exploitable](https://knowyourmeme.com/memes/exploitables) and [reaction face](https://knowyourmeme.com/memes/reaction-images).

[![Every fucking day. Every single fucking day. This little faggot just sits there and gives me this stupid fucking ook on his face Poodle dog dog like mammal dog breed snout photo caption](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "copypasta")](https://knowyourmeme.com/photos/1005337-my-faggot-dog)

  

#### Benton

**[Benton](https://knowyourmeme.com/memes/benton)** is a dog starring in a viral video where he chases a pack of deer while his owner screams his name repeatedly while attempting to catch him.

[![UNITED COLORS OF BENTON Golden Retriever Labrador Retriever Scottish Deerhound Lurcher Beagle Boxer German Shepherd Deer dog dog breed dog breed group dog like mammal snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "818.jpg")](https://knowyourmeme.com/photos/308498-dogs)

#### Stoner Dog

**[Stoner Dog](https://knowyourmeme.com/memes/stoner-dog)** is an image macro series featuring a picture of a dog that looks as if he is smiling. The background is typically adorned with [marijuana](https://knowyourmeme.com/memes/cultures/marijuana-stoner) leafs, and has smoke surrounding the dog’s face. The overlaid text generally makes some type of statement that could be associated with someone under the influence of marijuana.

[![FYOU SAYJESUS BACKWARDS IT SOUNDS LIKE SAUSAGE Dog dog dog breed dog like mammal dog breed group photo caption snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "893.jpg")](https://knowyourmeme.com/photos/308385-dogs)

#### Introspective Pug

[Introspective Pug](https://knowyourmeme.com/memes/introspective-pug) is an advice animal image macro series featuring a photograph of a pug dog looking out a window in the back seat of a car. The captions typically portray the dog as having a brooding, curious or [philosophical](https://knowyourmeme.com/memes/cultures/philosophy) internal monologue, often beginning with the phrase “I don’t know man, I just…”

[![DONT I KNOW MAN. JUST WHAT IF HE REALLY DID THROW THE TENNIS BALL, VOU KNOW? WeKnowMemes Pug pug dog dog like mammal photo caption mammal vertebrate snout dog breed](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "i don't know man, i just - what if he really did throw the tennis ball, you know?")](https://knowyourmeme.com/photos/413089)

#### Pun Dog

[**Pun Dog**](https://knowyourmeme.com/memes/pun-dog) is a three-panel image macro series featuring photographs of an Alaskan Klee Kai dog with animal-themed puns and anticlimactic punchlines, in a similar vein to the advice animal characters [Bad Joke Eel](https://knowyourmeme.com/memes/bad-joke-eel) and [Lame Pun Coon](https://knowyourmeme.com/memes/lame-pun-coon).

[![ISTHIS A REAL PUPPY OR NOT. Siberian Husky Alaskan Klee Kai Puppy dog dog like mammal siberian husky photo caption dog breed sakhalin husky dog breed group](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Ha")](https://knowyourmeme.com/photos/711379-pun-husky)

  

#### 6O163's "DOG"

[6O163's "DOG"](https://knowyourmeme.com/memes/6o163-s-dog) is a YouTube video uploaded by 6O163 which presents a continuously reflecting and rotating picture of a dog and a musical part of the ending theme song of the 1990’s SNES game [Super Mario](https://knowyourmeme.com/memes/subcultures/super-mario) World.

<iframe width="425" height="319" src="https://www.youtube.com/embed/y9K18CGEeiI" frameborder="0" allowfullscreen=""></iframe>

  

#### Financial Advice Dog

[**Financial Advice Dog**](https://knowyourmeme.com/memes/financial-advice-dog) is an advice animal image macro series featuring a Golden Retriever wearing eyeglasses and sitting at a desk with an open notebook. The captions juxtapose accounting jargons with canine puns, similar to those found in [Business Dog](https://knowyourmeme.com/memes/business-dog) and [Lawyer Dog](https://knowyourmeme.com/memes/lawyer-dog) images.

[!['VE CRUNCHED THE NUMBERS THIS MONTHISGOINGTOBE RUFF quickmeme.com mammal photo caption dog dog like mammal](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "ea6.jpg")](https://knowyourmeme.com/photos/480322)

  

#### Dogs Wearing Pantyhose

[**Dogs Wearing Pantyhose**](https://knowyourmeme.com/memes/dogs-wearing-pantyhose) (Chinese translation: 狗狗穿丝袜 or "Gou Gou Chuan Siwa") is a Chinese [photo fad](https://knowyourmeme.com/memes/cultures/photo-fads) in which dogs are photographed wearing black sheer tights.

[![Dog breed Dog Retriever Companion dog Sporting Group dog dog like mammal dog breed](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "e50.jpg")](https://knowyourmeme.com/photos/526209-gou-gou-chuan-siwa-dogs-wearing-pantyhose)

  

#### Texts From Dog

[**Texts from Dog**](https://knowyourmeme.com/memes/texts-from-dog) is a [single topic blog](https://knowyourmeme.com/memes/single-topic-blogs) featuring a series of fictional chat logs that supposedly take place between U.K.-based blogger October Jones and his pet bulldog. Since its launch in April 2012, Jones’ blog has been covered by several news publications and even turned into a book in October.

[![O2-UK 3G 22:20 Messages Dog Edit WANT THE BALL? YES. GIVE ME THE BALL WHO WANTS THE BALL? ME. GIVE ME THE BALL Well done. I've wet myself. HA! I'm on your bed FUCK. OText Message Send Dog Texts from Dog green text yellow font line](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "d8c.jpg")](https://knowyourmeme.com/photos/455902)

#### Noir Dog

**Noir Dog** is an advice animal image macro series featuring a black-and-white photograph of a dog staring out a window with captions depicting the canine as a brooding detective character typically associated with film noir.

[![OFALL THE FRONT YARDS NALL THE WORLD THE CAT WALKS INTO MINE Dog dog dog like mammal dog breed black and white photo caption nose snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "aae.jpg")](https://knowyourmeme.com/photos/851071-noir-dog)

  

#### Doge

**[Doge](https://knowyourmeme.com/memes/doge)** is a slang term for “dog” that is primarily associated with pictures of [Shiba Inus](https://knowyourmeme.com/memes/shiba-inus-shibes) (nicknamed “Shibe”) and internal monologue captions. The photos may be photoshopped to change the dog’s face or captioned with interior monologues in [Comic Sans](https://knowyourmeme.com/memes/comic-sans) font.

[![Shiba Inu Shih Tzu dog dog like mammal dog breed dog breed group shiba inu](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Doge - The Original Image")](https://knowyourmeme.com/photos/581296-doge)

#### Dogshaming

[**Dogshaming**](https://knowyourmeme.com/memes/sites/dogshaming) is a single topic blog featuring photographs of dogs with signs describing transgressions performed by the animal.

[![I AM AN UNDER EAR EATINGJERK Hi Dachshund Poodle Dog Shaming dog dog like mammal dog breed](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "d86.jpg")](https://knowyourmeme.com/photos/381954-dogshaming)

  

#### Dogs With Eyebrows

[**Dogs with Eyebrows**](https://knowyourmeme.com/memes/dogs-with-eyebrows) is a [photo fad](https://knowyourmeme.com/memes/photo-fads) in which dogs are photographed with eyebrows drawn on their faces. While some dogs are naturally born with eyebrow-like colorization, the fashion trend for pet dogs originated from East Asia in the early 2000s, as evidenced in an article published by the Chinese news site TVBS in March 2004 and a forum post submitted to the South Korean humor forum Bittalk on in October that year (shown below).

[![Shiba Inu Shar Pei dog dog like mammal](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "384.jpg")](https://knowyourmeme.com/photos/637802)

  

#### "Ahorita No, Joven"

["**Ahorita No, Joven**"](https://knowyourmeme.com/memes/ahorita-no-joven) (translated as _Not now, young man_ or _Not now, boy_) is an image macro series that features dogs in comical poses, gesturing, or performing human activities, just as with another canine macro, [Lucho](https://knowyourmeme.com/memes/oye-lucho-el-perro-del-lucho). The image macro is commonly used as a [reaction image](https://knowyourmeme.com/memes/reaction-images) to express indifference or dismissal, similar [GTFO](https://knowyourmeme.com/memes/gtfo) or [Stop Posting](https://knowyourmeme.com/memes/stop-posting).

[![NO JOVEN AHORITANO Shiba Inu Akita Pembroke Welsh Corgi Japan convenience store grocery store](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Not now, boy")](https://knowyourmeme.com/photos/972422)

  

#### Overly Excited Dog

[**Overly Excited Dog**](https://knowyourmeme.com/memes/overly-excited-dog) refers to a photoshop [meme](https://knowyourmeme.com/memes/memes) featuring a dog with an excited expression protecting a kitten on a brown couch. The meme became popular after the photograph of the dog was featured on Reddit. According to the poster, the dog is a mutt breed from a local shelter.

[![Dog Cat Kitten dog dog breed dog like mammal dog breed group borador](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Overly Excited Dog: Original Posting Companion Image")](https://knowyourmeme.com/photos/995330-overly-excited-dog) [![Dog Kitten Cat dog dog breed dog like mammal dog breed group borador snout](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Overly Excited Dog: Original ")](https://knowyourmeme.com/photos/995329-overly-excited-dog)

  

#### Sleep Tight Pupper

[**Sleep Tight Pupper**](https://knowyourmeme.com/memes/sleep-tight-pupper) is an ["If You See This While Scrolling"](https://knowyourmeme.com/memes/if-you-see-this-image-while-scrolling) image macro featuring a photograph of a Chihuahua dog wearing a hooded sweater while tucked into a bed, promising good night sleeps to whoever replies with the text "sleep tight pupper."

[![Anonymous Sat 30 May 2015 05:15:08 No.618661775 Quoted By: >>618662005 >>618662142 >>618662234 >>618662315 >>618662342 >>618662352 >>618662353 >618662369 >>618662504 >>618662518 >>618662649 >>618662803 >>618662828 >618662830 >>618662905 > >618662910 >>618662937 >>618662958 >>618663100 >>618663158 >>618663256 > >618663336 >>618663412 >>618663438 >>618663522 >>618663524 >>618663539 >618663630 >>618663701 >>618663767 >>618663810 >>618663822 >>618663841 > >618663928 >>618663936 >>618663937 >>618663946 >>618663975 > >618664077 > >618664126 >>618664181 >618664355 >>618664428 >618664476 > >618664522 > 618664575 >>618664704 >>618664723 >>618664756 >>618664791 >>618664869 >618664877 >>618664925 >>618664995 >>618665009 >618665012 >>618665110 >>618665122 >>618665398 >>618665501 >>618665556 >>618665559 >>618665675 > >618665706 >>618665756 >>618665883 >>618665974 >>618666145 >>618666240 >>618666377 >>618666445 >>618666494 >>618666569 >>618666585 > >618666651 > >618667119 >>618667188 >>618667189 >>618667241 >618667304 >>618667399 >>618667462 >>618667475 >>618667580 >>618667715 >>618668496 View Reply Original Report You've been visited by the Good Sleep Chihuahua. You will be blessed with cozy, restful sleeps, but only if you comment: sleep tight pupper 39KiB, 843x818, FB_IMG_1432955599371 jpg View Same Google iqdb SauceNA。 Sleep tight pupper Twilight Sparkle text joint](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "First instance on /b/")](https://knowyourmeme.com/photos/1008399-sleep-tight-pupper)

  

#### Tetsu the Robot Pet Dog

**"Tetsu" the Robot Pet Dog** (Japanese: 愛犬ロボ「てつ」, _Aiken Robo "Tetsu"_) is a battery-operated stuffed animal toy produced by the Japanese toy company Iwaya.[\[1\]](https://knowyourmeme.com/memes/subcultures/dogs#fn1) Because of its ridiculousness, the infomercial for this toy generated a lot of buzz among Japanese internet users and went on to spawn an extensive series of [MAD](https://knowyourmeme.com/memes/mad) videos on the Japanese video sharing site [Nico Nico Douga](https://knowyourmeme.com/memes/sites/niconico) (NND).

[![Dog dog like mammal dog text dog breed puppy product](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "5ad.jpg")](https://knowyourmeme.com/photos/345538)

#### Shoob

[**Shoob**](https://knowyourmeme.com/memes/shoob) is a slang term for a large, white herding dog known as a Samoyed. Known for it's fluffy white fur, Samoyed has received a similar treatment online to [doge or shibe.](https://knowyourmeme.com/memes/doge)

[![Samoyed dog Shiba Inu Dalmatian dog Maltese dog dog dog like mammal dog breed dog breed group german spitz](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Lana del shoober ")](https://knowyourmeme.com/photos/1261215) [![hehe I have no concept of numbers dog dog like mammal dog breed dog breed group pomeranian german spitz klein volpino italiano german spitz german spitz mittel japanese spitz](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "happy SHOOB DAY 2 U")](https://knowyourmeme.com/photos/1261216)

  

## Recent Videos (952)

<iframe frameborder="0" height="315" src="https://www.youtube.com/embed/" width="560"></iframe>

 [![Hqdefault](https://s.kym-cdn.com/assets/thumbnail_missing-9b6f4a707b0b7b5a8e095c7b21d230b2.png)](https://knowyourmeme.com/videos/362686-dogs) 

 [![Hqdefault](https://s.kym-cdn.com/assets/thumbnail_missing-9b6f4a707b0b7b5a8e095c7b21d230b2.png)](https://knowyourmeme.com/videos/362366-dogs) 

 [![Hqdefault](https://s.kym-cdn.com/assets/thumbnail_missing-9b6f4a707b0b7b5a8e095c7b21d230b2.png)](https://knowyourmeme.com/videos/361840-dogs) 

 [![Hqdefault](https://s.kym-cdn.com/assets/thumbnail_missing-9b6f4a707b0b7b5a8e095c7b21d230b2.png)](https://knowyourmeme.com/videos/361219-dogs) 

## Recent Images (2,315)

## Tags

## Additional References

## Entry Editors (10)

[Request Editorship](https://knowyourmeme.com/memes/dogs/editorships/ask)

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Z.")](https://knowyourmeme.com/users/z--4)

Database Moderator & Rage Comicologist

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "amanda b.")](https://knowyourmeme.com/users/amanda-b--3)

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Brad")](https://knowyourmeme.com/users/brad--12)

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "MScratch")](https://knowyourmeme.com/users/mscratch)

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Twenty-One")](https://knowyourmeme.com/users/twenty-one)

Anime Anthropologist & Protip Advisor & LGBT Community Expert

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Matt")](https://knowyourmeme.com/users/matt--68)

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Y F")](https://knowyourmeme.com/users/y-f)

Digital Archaeologist & Treasurer & Media Maid

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Don")](https://knowyourmeme.com/users/don--22)

Administrator & Meme Daddy

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "andcallmeshirley")](https://knowyourmeme.com/users/andcallmeshirley)

Digital Archaeologist & Treasurer & Collection Butler

[![](https://s.kym-cdn.com/assets/blank-8f52844e31c600b21b78a8b0b00dbfbb.gif "Iwazaru")](https://knowyourmeme.com/users/iwazaru)

Sr. Entry Moderator & Constable

## Comments

