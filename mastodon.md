#mastodon 

## Backlinks

> - [](masto.md)
>   - #same-as [[mastodon]]
>    
> - [](integration of the agora.md)
>   - Here's an outline for possible [[twitter]]/[[mastodon]] integration
>    
> - [](immer protocol.md)
>   - >This is the keystone of the [[metaverse]]. It connects Immersive Web experiences together by giving them a common language so that users (immersers) can bring their identities and avatars with them wherever they travel, and they can pick up friends and trophies along the way, but without dictating anything about how each world works or what technology it uses. Immersers will be able to broadcast their location to friends so that, with the simple click of a link, they can instantly join anywhere in [[immers space]]. We’re building it open-source, making it easy to plug in to all kinds of experiences, and using the ActivityPub open Web standard which means it will also [link with popular [[federated social media networks]] like [[mastodon]]](https://blog.joinmastodon.org/2018/06/why-activitypub-is-the-future/).
>    
> - [](pinafore.md)
>   - lightweight client for [[mastodon]]
>    
> - [](federated networks.md)
>   - [[mastodon]]
>    
> - [](agora graph.md)
>   - [[mastodon]] [[twitter]] etc are underlays
>    
> - [](2021-02-11.md)
>   - Started development on [[moa party]] for [[agora server]] integration. I'm going to take data from [[mastodon]]/[[twitter]] and shovel it over to a git repo via a git web interface probably using gitlab at first since that's our stoa org.
>    
> - [](moa.party.md)
>   - [[twitter]]-[[mastodon]] integration
>    
> - [](fediverse.md)
>   - Linked network of sites like [[mastodon]] [[pleroma]]

_Backlinks last generated 2022-01-28 07:55:20_
