[[hierarchy]]

## Backlinks

> - [](matrix spaces.md)
>   - Using Spaces and Subspaces to build [[hierarchies]] effectively turns [[matrix]] into a global decentralised [[filesystem]] for conversations and other real-time data!

_Backlinks last generated 2022-01-28 07:55:20_
