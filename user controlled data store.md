- [[ctzn]]
- [[diaspora]]
- [[fission]]
- [[solid]]

[//begin]: # "Autogenerated link references for markdown compatibility"
[ctzn]: ctzn.md "ctzn"
[solid]: solid.md "solid"
[//end]: # "Autogenerated link references"

## Backlinks

> - [](2021-04-19.md)
>   - I talked to [[flancian]] a lot about possibly using [[ctzn]] as a [[data store]] and identity layer. I need to research the [[whitepaper]] some more to learn the nuances

_Backlinks last generated 2022-01-28 07:55:20_
