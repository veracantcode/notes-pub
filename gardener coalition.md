- I've been discussing with people about forming an informal coalition of people who's project goals align with ours
- potential members
	- [[hypha coop]]
	- [[understory]]
		- [[understory call 2021-05-27]]
		- [[understory call 2021-06-04]]
	- [[fission]]
	- [[ctzn]]
	- [[zebras unite]]

- Alternate name could be [[data commons coalition]] or [[dcc]]

[//begin]: # "Autogenerated link references for markdown compatibility"
[hypha coop]: hypha coop.md "hypha coop"
[understory]: understory.md "understory"
[understory call 2021-05-27]: understory call 2021-05-27.md "understory call 2021-05-27"
[understory call 2021-06-04]: understory call 2021-06-04.md "understory call 2021-06-04"
[ctzn]: ctzn.md "ctzn"
[data commons coalition]: data commons coalition.md "data commons coalition"
[//end]: # "Autogenerated link references"

## Backlinks

> - [](wtf is the agora.md)
>   - [[gardener coalition]]

_Backlinks last generated 2022-01-28 07:55:20_
