- [Your First Extension | Visual Studio Code Extension
API](https://code.visualstudio.com/api/get-started/your-first-extension)
- colors for existing/non-existing
- push
- go to agora link
- add agora content for note in preview view

## Backlinks

> - [](2021-01-04.md)
>   - -	checking out [[dendron]] It's a [[vscode]] extension like [[foam]]
>    
> - [2022-01-17](arweave.md)
>   - Found a [[vscode]] [[extension | vscode extension]] called [insert date string](https://marketplace.visualstudio.com/items?itemName=jsynowiec.vscode-insertdatestring) that lets you insert an [[iso8601]] date using vscode shortcut
>    
> - [2022-01-15](2022-01-15.md)
>   - [[agora-vscode]]

_Backlinks last generated 2022-01-28 07:55:20_
