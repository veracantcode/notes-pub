- Things that signify a semantic place or object in the [[ideasphere]]. I wouldn't use "ball " as a concrete noun, even though it's a noun, but I would use [[twitter]]. concrete nouns are essentially things you would intuitively [[wikilink]]
- [[agora actions]] are not concrete nouns

[//begin]: # "Autogenerated link references for markdown compatibility"
[twitter]: twitter.md "twitter"
[agora actions]: agora actions.md "agora actions"
[//end]: # "Autogenerated link references"

## Backlinks

> - [](2021-01-11.md)
>   - [[concrete nouns]]

_Backlinks last generated 2022-01-28 07:55:20_
