

## Backlinks

> - [](scrying pen.md)
>   - [[project]] by [[Andy Matuschak]]
>    
> - [](spaced-repetition testing.md)
>   - from [[Andy Matuschak]]
>    
> - [](quantum.country.md)
>   - [[project]] [[Andy Matuschak]]
>    
> - [](speculative outline.md)
>   - idea by [[Andy Matuschak]]

_Backlinks last generated 2022-01-28 07:55:20_
