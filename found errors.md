Using this note to keep track of errors I need to google throughout my day.  
Maybe I can use this to train an [[AI]] someday or something?

# 2022-01-17
- `Warning: untrusted X11 forwarding setup failed: xauth key data not generated`
  - https://serverfault.com/questions/273847/what-does-warning-untrusted-x11-forwarding-setup-failed-xauth-key-data-not-ge
  - `Quite simply, the difference between -X and -Y is that -Y enables trusted X11 forwarding.`

## Backlinks

> - [](2022-01-17.md)
>   - -	[[found errors]]
>    
> - [](bookmarks.md)
>   - -	[[found errors]] (errors that I find)

_Backlinks last generated 2022-01-28 07:55:20_
