- pros
	- user controlled
- cons
	- if lost can not retrieve
	- needs an alias to be memorable
	- 

## Backlinks

> - [](global id.md)
>   - [[self sovereign identity]]
>    
> - [](logseq/pages/especifismo.md)
>   - might lead to interesting discussions on federated [[trust]] and [[identity]]

_Backlinks last generated 2022-01-28 07:55:20_
