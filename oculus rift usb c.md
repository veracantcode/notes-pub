- [[oculus rift s]] needs a [[display port]] to function properly, you can get the headset to work if your usb c port supports [[thunderbolt]]
- [[thunderbolt firmware update]]
- [[Thunder Bolt Control Center]]
- [(1) Oculus Rift S Laptop Issues, USB-C : oculus (reddit.com)](https://www.reddit.com/r/oculus/comments/bsdmt1/oculus_rift_s_laptop_issues_usbc/)

## Backlinks

> - [](2021-05-26.md)
>   - I ended up getting a display port to [[usb c]] adapter, we'll see if that works out
>    
> - [](vr headsets.md)
>   - [[oculus]]

_Backlinks last generated 2022-01-28 07:55:20_
