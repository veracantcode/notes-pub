# Vera's home on the web
You made it to my secret lair, congratulations! 🎉

# Social links
insta: moonlion.eth  
venmo/cashapp: verahunny  
annotations: https://annotations.lindylearn.io/@verapetrova/

# Virtual places I frequent
-	[Gather](https://gather.town/app/UIn5AAlVh3IUSKiP/home)
-	[vera @Freethinkers.lgbt](https://vera@freethinkers.lgbt)
-	[[matrix]] vera@fairydust.space
-	[fuck-capitalism · GitHub](https://github.com/fuck-capitalism)
-	[twitter](https://twitter.com/moonlion_eth)

# Go links
- [[go]] 
	- [[books]] https://vera.files.fission.name/p/books


# Tags
-	#developer
-	#protopian
-	#anarchist

# Experimental features
[[agora pull]] [[vera]]



Wow it took this long to make a readme



