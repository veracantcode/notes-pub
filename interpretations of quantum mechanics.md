- [[copenhagen interpretation of quantum mechanics]]
- [[many worlds interpretation of quantum mechanics]]

## Backlinks

> - [what do we want from the [[agora]] interlay?](2021-W23.md)
>   - [[interpretations of quantum mechanics]]
>    
> - [](implications of quantum mechanics on philosophy.md)
>   - [[interpretations of quantum mechanics]] are strictly not-science

_Backlinks last generated 2022-01-28 07:55:20_
