#go https://en.wikipedia.org/wiki/Born_rule

In its simplest form, it states that the probability density of finding a particle at a given point, when measured, is proportional to the square of the magnitude of the particle's [wavefunction](https://en.wikipedia.org/wiki/Wavefunction "Wavefunction") at that point

## Backlinks

> - [](self-locating uncertainty.md)
>   - [[born-rule]]
>    
> - [](notes on quantum mechanics.md)
>   - [[born-rule]]

_Backlinks last generated 2022-01-28 07:55:20_
