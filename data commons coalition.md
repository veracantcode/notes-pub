- co·a·li·tion
noun
an alliance for combined action, especially a temporary alliance of political parties forming a government or of states.


## Backlinks

> - [](gardener coalition.md)
>   - Alternate name could be [[data commons coalition]] or [[dcc]]

_Backlinks last generated 2022-01-28 07:55:20_
