-	besides not being the prettiest, it is very fast and responsive
-	startup time is amazing
-	I've configured it so that it looks presentable
-	so "single click to activate items" just means enable tap, ugh I hate developers sometimes
-	uses [[lxqt-runner]] to run one-off commands, I actually quite like it
-	seems to work well with [[kwin]] [[window manager]] from [[kde]]
	-	both of them use [[Qt]] under the hood, which I kinda prefer over [[gtk]] 

## Backlinks

> - [](2021-01-19.md)
>   - I'm trying out [[lxqt]] which is fast but kinda ugly. I wish designers were more into lightweight [[desktop environment]]s

_Backlinks last generated 2022-01-28 07:55:20_
