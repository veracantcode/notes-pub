essentially anything wikilinkable. words that form a concrete concept that is describable by multiple people in a similar manner

## Backlinks

> - [](radical cooperation.md)
>   - a phrase I made up to convery the [[semantic idea]] of not just collaborating with someone in a business/professional sense but a very viceral sense. The kind of collaboration people are willing to die over.

_Backlinks last generated 2022-01-28 07:55:20_
