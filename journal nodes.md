[[daily note]]

## Backlinks

> - [](node categories.md)
>   - [[agora]] node has many incoming links, whereas [[journal nodes]] tend to have many outgoing links

_Backlinks last generated 2022-01-28 07:55:20_
