- [Unwalled.Garden | An open protocol for building social Web applications.](https://unwalled.garden/)
- >Unwalled.Garden is a kind of “Souped up RSS.” Every user has a website, they publish their content as files, and they subscribe to each others’ sites.

## Backlinks

> - [](meetings 2021-05-19.md)
>   - [[unwalled.garden]]

_Backlinks last generated 2022-01-28 07:55:20_
