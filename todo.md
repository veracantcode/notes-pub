# 2022-01-27
- [ ] look at tiddlywiki parser
# 2022-01-26
- [ ] agora flavored markdown
- [ ] watch [[Contagion]]

# 2022-01-25
- [ ] fix [[wikilink | aliases]] in the agora

# 2022-01-22
Realized I haven't touched this file in awhile. Going to start using it again
- [ ] ensure [[wikilinks everywhere]] is up to date in firefox/chrome store


# Back burner
- [ ] check out obsidian plugin api
- [ ] move to proper [[ast]] instead of hacking around [[markdown]]
- [ ] [[agora obsidian plugin]]
- [ ] notion api
- [ ] investigate [[kava]]
- [ ] open web grant
- [ ] make graph view look more like obsidian
- [ ] add hypothesis integration into [[agora-server]] on a deeper level
- [ ] autogen journal entry if non existent for nodes created on that day
- [ ] "snapshot" feature to "freeze" a node at a specific time and archive
- [ ] make graph on agora prettier
- [ ] create chrome browser extension based on [[firefox]]
- [ ] check out sections on ctznry
- [ ] install [[mycroft]]
- [ ] dinner with [[aphid]]
- [ ] show actual journal entries in /journal route
	- can I use same codebase?
- [ ] build [[notion]] integration
- [ ] watch [[world at war]]
- [ ] make graph view look more like obsidian
- [ ] add hypothesis integration into [[agora-server]] on a deeper level
- [ ] autogen journal entry if non existent for nodes created on that day
- [ ] "snapshot" feature to "freeze" a node at a specific time and archive
- [ ] make graph on [[agora]] prettier
- [ ] create chrome browser extension based on [[firefox]]
- [ ] check out sections on ctznry
- [ ] install [[mycroft]]
- [ ] dinner with [[aphid]]
- [ ] show actual journal entries in /journal route
	- can I use same codebase?
- [ ] build [[notion]] integration
- [ ] watch [[world at war]]

# Done
- [x] create JS settings modules
- [x] build [[agora proposal platform]]
	- thinking of using tools at [[open source democracy]]
- [x] find a [[governance]] platform for [[agora proposals]]
	- Tools listed under [[open source democracy]]
- [x] cancel [[zoom]] phone if it doesn't get fixed
- [x] get grinder
- [x] 15 to tanya
- [x] update [[agora ctzn]]
- [x] lunch with [[jana]]Inbox
- [ ] create JS settings modules
- [ ] create branch for proof of concept for using [[fission]] for user login
- [ ] check out obsidian plugin api
- [ ] investigate [[kava]]
- [ ] move to proper [[ast]] instead of hacking around [[markdown]]
- [ ] [[agora obsidian plugin]]
- [ ] [[hyperbee]] to store agora data, 
	- we could possibly run into scaling issues running flat files, 
	- may be easier to dispatch [[agora actions]]
- [x] [[agora ext]]
	- [x] autolink only element you click on
	- [x] on/off toggle in toolbar
- [x] build [[agora proposal platform]]
	- thinking of using tools at [[open source democracy]]
- [x] find a [[governance]] platform for [[agora proposals]]
	- Tools listed under [[open source democracy]]
- [x] cancel [[zoom]] phone if it doesn't get fixed
- [x] get grinder
- [x] 15 to tanya
- [x] update [[agora ctzn]]
- [x] lunch with [[jana]]

