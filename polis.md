- Polis is a real-time system for gathering, analyzing and understanding what large groups of people think in their own words, enabled by advanced statistics and machine learning.
- #go https://pol.is/home
- #open-goverance 

## Backlinks

> - [](patcon 2021-05-21.md)
>   - [[polis]]

_Backlinks last generated 2022-01-28 07:55:20_
