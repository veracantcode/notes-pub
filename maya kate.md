- [[person]]
- [[go]] https://maya.land/who/
-  [maya 🧙‍♀️ (@maya@occult.institute) - Conventicle of the Lesser Occult Institute](https://occult.institute/@maya)
- [[blogroll]] [Maya's subscriptions](https://maya.land/blogroll.opml)
- [[swamp-person]]
- talking about if publishing notes to the public makes you write private notes less. I guess I'll find out
- They're acquainted with [[Aaron Parecki]] of [[indieweb]] fame, one of my old homies
- there is this way that [john locke](https://fs.blog/2014/07/john-locke-common-place-book/) organized his commonplace book that I found extremely quaint before coming across a slightly more modern version of it in the Great Books' [syntopicon](https://en.wikipedia.org/wiki/A_Syntopicon)
- the idea that you can boil down topics to neat addressable words that will mean enough-the-same-thing-to-be-useful across wide fields of discourse
- presenting different versions alongside each other allows a certain amount of... I remember consulting urban dictionary as a teenager trying to figure out a piece of slang, and feeling like I needed to see various slightly-different definitions to be confident something was even really in use
- They showed me  [proposal for a web made of markdown documents](https://macwright.com/2020/08/22/clean-starts-for-the-web.html?s=09 "https://macwright.com/2020/08/22/clean-starts-for-the-web.html?s=09")

## Backlinks

> - [](transcranial magnetic stimulation.md)
>   - learned about from [[maya kate]]
>    
> - [](2021-01-02.md)
>   - met [[maya kate]] and talked about [[folkpunk]] and [[agora]] and simpler web interfaces in general

_Backlinks last generated 2022-01-28 07:55:20_
